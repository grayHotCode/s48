import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {Fragment} from 'react'
import Error from '../pages/Error'

export default function Banner({data}){

	const {title, content, destination, label} = data;

	return(
	
	<Row>	
		<Col className = "p-5">
			<h1>{title}</h1>
			<p>{content}</p>
			<Link to={destination}>{label}</Link>
		</Col>
	</Row>
	
	)
}
