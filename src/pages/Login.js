import {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'

export default function Register(){

	// State hooks to store the values of the input fields
	const [password, setPassword] = useState('')
	const [email, setEmail] = useState('')

	// State to determine whether register button is enabled or not
	const [isActive, setIsActive] = useState(false)

	//check if values are succesfully binded 
	console.log(email)
	console.log(password)

	// 
	function authenticate(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// Set the email of the user in the local storage
		// Syntax: localStorage.setItem('propertyName', value);
		localStorage.setItem('email', email);

		// Clears the input field
		setEmail('');
		setPassword('');

		alert('You are now logged In');
	}



	useEffect(() => {
		if(email !== '' && password !== ''){

			setIsActive(true)

		}else{

			setIsActive(false)

		}

	}, [email, password])

	return(
		<Form onSubmit= {e=>authenticate(e)}>

			<Form.Text> Log In</Form.Text>
			<Form.Group>

				<Form.Label> Email Address:</Form.Label>			
				<Form.Control 
					type= 'email'
					placeholder= 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className= "text-muted">
					Well never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password: </Form.Label>
				<Form.Control 
					type='password'
					placeholder='Please input your password here'
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>


			{/*? if else :*/}
			{isActive ? 
				<Button variant='success' type= 'submit' id='submitBtn'>
							Submit
				</Button>

				:
				<Button variant='secondary' type= 'submit' id='submitBtn' disabled>
							Submit
				</Button>
			}

		</Form>


		)
}

// Ternary operator with the Button Tag
// ?- if -- if<Button isActive, the color will be primary and it is enabled>
// :- else -- <Button will be color danger and will be disabled>









